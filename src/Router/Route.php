<?php

namespace Orchestra\Router;

use Orchestra\Exceptions\InvalidRouteCallback;

class Route
{
   protected $router;

   protected $endpoint;

   protected $method;

   protected $callback;

   protected $attributes;

   public function __construct(Router $router, string $endpoint, string $method, $callback)
   {
      $this->router = $router;

      $this->endpoint = $endpoint;

      $this->method = $method;

      $this->callback = $callback;
   }

   public function __get($attribute)
   {
      return $this->{$attribute};
   }

   public function run()
   {
      $this->attributes = $this->getAttributes();

      $this->runBeforeCallbacks();

      $callback = $this->callback;

      return $this->runCallback($callback);
   }

   private function runBeforeCallbacks()
   {
      if (empty($this->router->getAttributes()["before"])) {
         return;
      }

      $beforeCallbacks = $this->router->getAttributes()["before"];

      foreach ($beforeCallbacks as $callback) {

         $this->runCallback($callback);
      }
   }

   private function runCallback($callback)
   {
      if (empty($callback)) {
         throw new InvalidRouteCallback("Empty before callback.");
      }

      if ($callback instanceof \Closure) {
         return $callback($this->attributes);
      }

      if (!class_exists($callback)) {
         throw new InvalidRouteCallback("Invalid before callback.");
      }

      $callback = new $callback;

      return $callback($this->attributes);
   }

   private function getAttributes()
   {
      $regex = '/{(.*?)}/';

      preg_match_all($regex, $this->endpoint, $matches);

      $attributes = [];

      $route = preg_replace($regex, '*', $this->endpoint);

      $routeWildcards = explode('/', $route);

      $urlParameters = explode("/", strtok(trim($_SERVER["REQUEST_URI"], "/"), '?'));

      $j = 0;

      for ($i = 0; $i < count($routeWildcards); $i++) {

         if ($routeWildcards[$i] == '*') {
            $attributes[$matches[1][$j]] = $urlParameters[$i];

            $j++;
         }
      }

      return array_merge($_REQUEST, $attributes);
   }
}
