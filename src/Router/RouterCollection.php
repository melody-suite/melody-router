<?php

namespace Orchestra\Router;

use Orchestra\Exceptions\RouteNotFoundException;

class RouterCollection
{
   private $routes = [];

   private static $instance;

   private function __construct()
   {
   }

   private static function getInstance()
   {
      if (empty(self::$instance)) {
         self::$instance = new RouterCollection();
      }

      return self::$instance;
   }

   public static function addRoute(Route $route)
   {
      $instance = self::getInstance();

      $instance->routes[] = $route;
   }

   public static function resolve($path, $method)
   {
      $instance = self::getInstance();

      $path = strtok(trim($path, '/'), '?');

      $requestedMethod = $method;

      foreach ($instance->routes as $route) {
         $endpoint = trim(self::getWildcardRoute($route->endpoint), '/');

         if (fnmatch($endpoint, $path, FNM_PATHNAME) && $route->method == $requestedMethod) {

            return $route->run();
         }
      }

      throw new RouteNotFoundException("Route for $path with $requestedMethod method not found.", 404);
   }

   private static function getWildcardRoute($path)
   {
      $regex = '/{(.*?)}/';

      return preg_replace($regex, '*', $path);
   }
}
