<?php

namespace Orchestra\Router;

use Orchestra\Exceptions\InvalidRouteMethodException;
use Orchestra\Exceptions\RouteEndpointNotSpecifiedException;

class Router
{
   protected $methods = ["GET", "POST", "PUT", "PATCH", "DELETE"];

   protected $attributes = [];

   public function get(string $endpoint, $callback)
   {
      $this->addRoute($endpoint, "GET", $callback);
   }

   public function post(string $endpoint, $callback)
   {
      $this->addRoute($endpoint, "POST", $callback);
   }

   public function put(string $endpoint, $callback)
   {
      $this->addRoute($endpoint, "PUT", $callback);
   }

   public function patch(string $endpoint, $callback)
   {
      $this->addRoute($endpoint, "PATCH", $callback);
   }

   public function delete(string $endpoint, $callback)
   {
      $this->addRoute($endpoint, "DELETE", $callback);
   }

   public function addRoute(string $endpoint, string $method = "GET", $callback)
   {
      RouterCollection::addRoute($this->getNewRouteInstance($endpoint, $method, $callback));
   }

   public function group(array $attributes, \Closure $callback)
   {
      if (!empty($this->attributes)) {

         $attributes = array_merge_recursive($this->attributes, $attributes, [
            "prefix" => $this->getPrefix($this->attributes) . "/" . $this->getPrefix($attributes),
         ]);
      }

      $this->attributes = $attributes;

      $callback($this);
   }

   public function getAttributes()
   {
      return $this->attributes;
   }

   private function getNewRouteInstance(string $endpoint, string $method, $callback)
   {
      if (empty($endpoint)) {
         throw new RouteEndpointNotSpecifiedException("Endpoint not specified.");
      }

      if (!in_array($method, $this->methods)) {
         throw new InvalidRouteMethodException("Endpoint not specified.");
      }

      $endpoint = trim($this->getPrefix($this->attributes) . "/" . $endpoint, "/");

      return new Route($this, $endpoint, $method, $callback);
   }

   private function getPrefix(array $attributes)
   {
      if (empty($attributes["prefix"])) {
         return '';
      }

      return trim($attributes["prefix"], '/');
   }
}
