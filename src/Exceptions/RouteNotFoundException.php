<?php

namespace Orchestra\Exceptions;

class RouteNotFoundException extends RequestException
{
}
