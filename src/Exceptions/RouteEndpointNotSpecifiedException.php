<?php

namespace Orchestra\Exceptions;

class RouteEndpointNotSpecifiedException extends \Exception
{}